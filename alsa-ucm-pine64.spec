%global alsa_ucm_commit a23b5ff2b32eca9542b4efb5d2522844076173a1

Name:           alsa-ucm-pine64
Version:        0.18.3
Release:        2.biktorgj%{?dist}
Summary:        Pine64 ALSA UCM configuration

License:        BSD-3-Clause
URL:            https://github.com/Biktorgj/pine64-alsa-ucm

Source0:        https://github.com/Biktorgj/pine64-alsa-ucm/archive/%{alsa_ucm_commit}.tar.gz
BuildArch:      noarch

%description
Experimental Pine64 ALSA UCM configuration from Biktorgj
that aims to Bluetooth calls. Fur full functionality you
will also need Biktorg's branches of libcall-ui (in calls)
and callaudiod.

%package pinephone
Summary:        PinePhone ALSA UCM configuration

%description pinephone
Experimental PinePhone ALSA UCM configuration from
Biktorgj that (will in the future) support Bluetooth calls.
Fur full functionality you will also need Biktorg's branches
of libcall-ui (in calls) and callaudiod.

%package pinephone-pro
Summary:        PinePhone Pro ALSA UCM configuration
Provides:	alsa-ucm-pinephone-pro
Obsoletes:	alsa-ucm-pinephone-pro <= 0.18.2-0.biktorgj

%description pinephone-pro
Experimental PinePhone Pro ALSA UCM configuration from
Biktorgj that is rewritten from scratch and supports
Bluetooth calls. Fur full functionality you will also
need Biktorg's branches of libcall-ui (in calls) and
callaudiod.

%global debug_package %{nil}

%prep
%setup -q -n pine64-alsa-ucm-%{alsa_ucm_commit}
sed -i 's|"EnableSeq.conf"|"/Pine64/PinePhonePro/EnableSeq.conf"|' ucm2/PinePhonePro/{HiFi,VoiceCall}.conf
sed -i 's|"DisableSeq.conf"|"/Pine64/PinePhonePro/DisableSeq.conf"|' ucm2/PinePhonePro/{HiFi,VoiceCall}.conf
sed -i 's|"HiFi.conf"|"/Pine64/PinePhone/HiFi.conf"|' ucm2/PinePhone/PinePhone.conf
sed -i 's|"VoiceCall.conf"|"/Pine64/PinePhone/VoiceCall.conf"|' ucm2/PinePhone/PinePhone.conf

%install
mkdir -p %{buildroot}/usr/share/alsa/
cp -a ucm2 %{buildroot}/usr/share/alsa/
pushd %{buildroot}/usr/share/alsa/ucm2/
mkdir Pine64
mv PinePhone PinePhonePro Pine64
ln -s Pine64/PinePhonePro PinePhonePro
ln -s Pine64/PinePhone PinePhone
mkdir -p conf.d/simple-card/
ln -s ../../Pine64/PinePhonePro/PinePhonePro.conf conf.d/simple-card/PinePhonePro.conf
ln -s ../../Pine64/PinePhonePro/PinePhonePro.conf conf.d/simple-card/PINE64-PinePhonePro-.conf
ln -s ../../Pine64/PinePhonePro/PinePhonePro.conf conf.d/simple-card/pine64-Pine64PinePhonePro-.conf
ln -s ../../Pine64/PinePhone/PinePhone.conf conf.d/simple-card/PinePhone.conf
ln -s ../../Pine64/PinePhone/PinePhone.conf conf.d/simple-card/PINE64-PinePhone-.conf
ln -s ../../Pine64/PinePhone/PinePhone.conf conf.d/simple-card/pine64-Pine64PinePhone-.conf
popd

%files
%doc LICENSES/BSD-3-Clause.txt

%files pinephone
%{_datadir}/alsa/ucm2/Pine64/PinePhone
%{_datadir}/alsa/ucm2/PinePhone
%{_datadir}/alsa/ucm2/conf.d/simple-card/PinePhone.conf
%{_datadir}/alsa/ucm2/conf.d/simple-card/PINE64-PinePhone-.conf
%{_datadir}/alsa/ucm2/conf.d/simple-card/pine64-Pine64PinePhone-.conf

%files pinephone-pro
%{_datadir}/alsa/ucm2/Pine64/PinePhonePro
%{_datadir}/alsa/ucm2/PinePhonePro
%{_datadir}/alsa/ucm2/conf.d/simple-card/PinePhonePro.conf
%{_datadir}/alsa/ucm2/conf.d/simple-card/PINE64-PinePhonePro-.conf
%{_datadir}/alsa/ucm2/conf.d/simple-card/pine64-Pine64PinePhonePro-.conf

%changelog
* Tue May 14 2024 marcin <marcin@ipv8.pl> - 0.18.2-2.biktorgj
- Add new names for some change in DTBs or something

* Fri Nov 25 2022 marcin <marcin@ipv8.pl> - 0.18.2-1.biktorgj
- Refactor again, to also build a similar package for PP

* Fri Nov 25 2022 marcin <marcin@ipv8.pl> - 0.18.2-0.biktorgj
- Refactor this in terms of Biktorg's git repo

* Wed Nov 23 2022 marcin <marcin@ipv8.pl> - 0.16.0.2022.11.23-0.biktorgj
- Update UCMs to the latest provided by Biktor

* Tue Nov 22 2022 marcin <marcin@ipv8.pl> - 0.16-0.biktorgj
- Update UCMs to the latest provided by Biktor

* Fri Nov 18 2022 marcin <marcin@ipv8.pl> - 0.0.2022.11.21-0.biktorgj
- Update UCMs to the latest provided by Biktor

* Fri Nov 18 2022 marcin <marcin@ipv8.pl> - 0.0.15.1-1.biktorgj
- Add a PINE64-PinePhonePro-.conf symlink for UEFI systems booted with the latest Tow-Boot to catch the config instantaneously

* Fri Nov 18 2022 marcin <marcin@ipv8.pl> - 0.0.15.1-0.biktorgj
- Initial version
